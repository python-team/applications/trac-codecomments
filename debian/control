Source: trac-codecomments
Section: web
Priority: optional
Build-Depends: python-all (>= 2.6.6-3~), python-setuptools, debhelper (>= 7.0.50~), yui-compressor
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: W. Martin Borgert <debacle@debian.org>
Standards-Version: 3.9.3.1
Vcs-Git: https://salsa.debian.org/python-team/packages/trac-codecomments.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/trac-codecomments
Homepage: https://github.com/Automattic/trac-code-comments-plugin

Package: trac-codecomments
Architecture: all
Depends: ${python:Depends}, ${misc:Depends}, trac, libjs-backbone, libjs-jquery-ui, libjs-json, libjs-underscore
Description: code comments and review plugin for Trac
 This plugin allows you to leave comments on top of files, changesets,
 and attachments. Once you've added all of your comments, you can send
 them to tickets. These include links to these comments and their
 description.
  * Comments on files – you can comment on every file in the
    repository.
  * Inline comments on files – comment on a specific line. The
    comments appears in context, below the line in question.
  * Comments on changesets – useful when doing code reviews of
    incoming commits.
  * Comments on attachment pages – useful when reviewing patches.
  * Wiki Markup – you can use the standard Trac wiki markup inside
    your comments.
  * Instant preview – to make sure you get the formatting right.
  * Sending comments to tickets – you can select arbitrary number of
    comments and create a new ticket out of them. The text of the
    ticket defaults to links to the comments and their text, but you
    can edit these before saving the ticket.
  * Comments/ticket cross-reference – to remember which comments are
    already in tickets and which are not.
